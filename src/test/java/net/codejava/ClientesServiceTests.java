package net.codejava;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collection;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;


@DataJpaTest(includeFilters = @ComponentScan.Filter(Service.class))
public class ClientesServiceTests {

	@Autowired
	private ClientesRepository clienteRepo;
	
	
	@Test
	void shouldFindClienteByName() {
		Collection<Clientes> cliente = this.clienteRepo.findByName("Dojo");
		assertThat(cliente).hasSize(1);

		cliente = this.clienteRepo.findByName("D0j0");
		assertThat(cliente).isEmpty();
	}

	@Test
	void shouldFindSingleCliente() {
		Clientes cliente = this.clienteRepo.getOne(1L);
		assertThat(cliente.getName()).startsWith("Dojo");
	}
	
	@Test
	void shouldDeleteCliente() {
		Optional<Clientes> cliente1 = this.clienteRepo.findById(1L);
		this.clienteRepo.delete(cliente1.get());
		Optional<Clientes> cliente2 = this.clienteRepo.findById(1L);
		
		assertThat(cliente2).isEmpty();
	}
	
	@Test
	void shouldSaveCliente() {
		Clientes cliente = new Clientes(10L, "Hercules", "Griego", "El Salvador", 1.85f);
		this.clienteRepo.save(cliente);
		assertThat(cliente).isNotNull();
	}
	
}
