package net.codejava;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ClientesIntegrationTests {

	@Autowired
	private ClientesRepository clienteRepo;
	
	@Test
	void testFindAll() throws Exception {
		clienteRepo.findAll();
	}

	@Test
	void testByCreate() throws Exception {
		clienteRepo.save(new Clientes(10L, "Emergy", "Kirio", "El Salvador", 1.85f));
	}

	@Test
	void testByDelete() throws Exception {
		clienteRepo.deleteById(1L);
	}

}
