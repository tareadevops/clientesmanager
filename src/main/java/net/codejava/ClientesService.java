package net.codejava;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ClientesService {

	@Autowired
	private ClientesRepository repo;
	
	public List<Clientes> listAll() {
		return repo.findAll();
	}
	
	public void save(Clientes cliente) {
		repo.save(cliente);
	}
	
	public Clientes get(long id) {
		return repo.findById(id).get();
	}
	
	public void delete(long id) {
		repo.deleteById(id);
	}
}
