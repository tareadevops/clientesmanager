package net.codejava;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ClientesController {

	@Autowired
	private ClientesService service; 
	
	@RequestMapping("/")
	public String viewHomePage(Model model) {
		List<Clientes> listClientes = service.listAll();
		model.addAttribute("listClientes", listClientes);

		return "index";
	}
	
	@RequestMapping("/clientes/new")
	public String showNewClientePage(Model model) {
		Clientes cliente = new Clientes();
		model.addAttribute("cliente", cliente);
		
		return "new_cliente";
	}
	
	@RequestMapping(value = "/clientes/save", method = RequestMethod.POST)
	public String saveCliente(@ModelAttribute("cliente") Clientes cliente) {
		service.save(cliente);
		
		return "redirect:/";
	}
	
	@RequestMapping("/clientes/edit/{id}")
	public ModelAndView showEditClientePage(@PathVariable(name = "id") int id) {
		ModelAndView mav = new ModelAndView("edit_cliente");
		Clientes cliente = service.get(id);
		mav.addObject("cliente", cliente);
		
		return mav;
	}
	
	@RequestMapping("/clientes/delete/{id}")
	public String deleteCliente(@PathVariable(name = "id") int id) {
		service.delete(id);
		return "redirect:/";		
	}
}
